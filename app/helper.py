def hello_world() -> str:
    return "Hello world!"


def add(num_1: int, num_2: int) -> int:
    return num_1 + num_2
